const images = document.querySelectorAll('img')
const button = document.querySelector('button')
const timer = document.querySelector('#timer')

const timerNumbers = timer.textContent.split('').map(Number)
console.log(timerNumbers)
timerNumbers[2] = ':'

let counter = 0
let isRunning = false
const NEXT_IMAGE_TIME = 3000

button.addEventListener('click', () => {
	// isRunning === true ? (isRunning = false ) : showImageTimer(images, counter)
	if (isRunning === true) {
		isRunning = false
		button.textContent = 'Відновити показ'
	} else {
		button.textContent = 'Припинити'
		showImageTimer(images, counter)
	}
})

for (const image of images) {
	image.style.opacity = '0'
}
setInterval(() => {
	if (isRunning === true) {
		timerNumbers[3]--
		timer.textContent = timerNumbers.join('')
		if (timerNumbers[3] <= 0) {
			timerNumbers[1]--
			if (timerNumbers[1] < 0 && timerNumbers[3] <= 0) {
				timerNumbers[1] = 3
				timerNumbers[3] = 1
				return
			}
			timerNumbers[3] = 9
		}
	}
}, 110)
const showImageTimer = (imageArray, counter) => {
	isRunning = true
	if (counter === imageArray.length) {
		setTimeout(() => {
			for (const image of imageArray) {
				image.style.opacity = '0'
			}
			showImageTimer(imageArray, 0)
		}, NEXT_IMAGE_TIME)
		return
	}
	imageArray[counter].style.opacity = '1'
	setTimeout(() => {
		if (counter === imageArray.length - 1) {
			for (const image of imageArray) {
				image.style.opacity = '0'
			}
		}
		if (isRunning === false) {
			return
		}
		showImageTimer(imageArray, counter + 1)
	}, NEXT_IMAGE_TIME)
}

showImageTimer(images, counter)

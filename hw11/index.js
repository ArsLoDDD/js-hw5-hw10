const passIcons = document.querySelectorAll('.input-wrapper i')
const passInputs = document.querySelectorAll('.input-wrapper input')
const btn = document.querySelector('.btn')

let inCount = 0

for (const passIcon of passIcons) {
	let iconIndex = inCount
	inCount++
	passIcon.addEventListener('click', event => {
		if (event.target.classList.contains('fa-eye-slash')) {
			event.target.classList.replace('fa-eye-slash', 'fa-eye')
			changeInputType(passInputs[iconIndex], event.target)
		} else {
			event.target.classList.replace('fa-eye', 'fa-eye-slash')
			changeInputType(passInputs[iconIndex], event.target)
		}
	})
}

changeInputType = (input, icon) => {
	if (input.type === 'password' && icon.classList.contains('fa-eye')) {
		input.type = 'text'
	} else if (input.type === 'text' && icon.classList.contains('fa-eye-slash')) {
		input.type = 'password'
	}
}

allowedPass = (passFrst, passScd) => {
	if (passFrst === passScd && (passFrst.length && passScd.length) >= 6) {
		return true
	} else {
		return false
	}
}
btn.addEventListener('click', () => {
	if (allowedPass(passInputs[0].value, passInputs[1].value)) {
		alert('You are welcome')
	} else {
		alert('Потрібно ввести однакові значення')
		passInputs[1].style.border = '1px solid red'
	}
})

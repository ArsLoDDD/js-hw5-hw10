const bodyElem = document.querySelector('body')
console.log(document.body)
const array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
const scdArray = [
	'Kharkiv',
	'Kiev',
	['Borispol', 'Irpin'],
	'Odessa',
	'Lviv',
	'Dnieper',
]

const addArrayToPage = (array, pageItem = document.body) => {
	const newList = document.createElement('ul')
	array.forEach(item => {
		if (Array.isArray(item)) {
			return addArrayToPage(item, newList)
		}
		const newListItem = document.createElement('li')
		newListItem.innerHTML = item
		return newList.appendChild(newListItem)
	})
	return pageItem.appendChild(newList)
}

const clearPage = () => {
	bodyElem.innerHTML = ''
}

const timerElem = document.createElement('div')
timerElem.style.fontSize = '24px'
timerElem.style.textAlign = 'center'
bodyElem.appendChild(timerElem)

let counter = 3
timerElem.innerText = counter

const intervalId = setInterval(() => {
	counter--
	timerElem.innerText = counter
	if (counter === 0) {
		clearInterval(intervalId)
		clearPage()
	}
}, 1000)

console.log(addArrayToPage(array))
console.log(addArrayToPage(scdArray))

//1. функция внутри объекта/класса которая выполняет определенные манипуляции со значениями класса
//2. Любой тип данных
//3. это определенный внутренний тип который передает информацию от точки до вызова скобок

const createNewUser = () => {
  const frstName = prompt('First name')
  const lastName = prompt('Last name')
  const newUser = {
    _frstName: frstName,
    _lastName: lastName,
    /**
     * @param {any} valuex
     */
    set frstName(value) {
      return this._frstName = value
    },
    set lastName(value) {
      return this._lastName = value
    },
    getLogin() {
      return (this._frstName.substring(0, 1) + this._lastName).toLowerCase()
    },

  }
  return newUser
}
const user = createNewUser()
console.log(user.getLogin());
user.frstName = 'Arseniy'
user.lastName = 'Shkolnikov'
console.log(user.getLogin());


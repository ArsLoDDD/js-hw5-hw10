const allListItemTabNames = document.querySelectorAll('.tabs-title')
const allTabsContenListItem = document.querySelectorAll('.tabs-content li')
console.log(allTabsContenListItem)
allListItemTabNames[0].classList.add('active')
for (let i = 1; i < allTabsContenListItem.length; i++) {
	allTabsContenListItem[i].classList.add('hide')
}

changeActiveTab = array => {
	for (const itemTab of array) {
		itemTab.addEventListener('click', () => {
			if (itemTab.classList.contains('active')) {
				return
			}
			clearClassActive(array)
			itemTab.classList.add('active')
			console.log()
			findContentText(
				allTabsContenListItem,
				findIndexName(array, itemTab.textContent)
			)
		})
	}
}

clearClassActive = array => {
	for (const classActive of array) {
		classActive.classList.remove('active')
	}
}

findIndexName = (array, name) => {
	for (let i = 0; i < array.length; i++) {
		if (array[i].textContent === name) {
			return i
		}
	}
}

findContentText = (array, index) => {
	for (let i = 0; i < array.length; i++) {
		const element = array[i]
		if (i === index) {
			element.classList.remove('hide')
			continue
		}
		if (element.classList.contains('hide') === false) {
			element.classList.add('hide')
		}
	}
}

changeActiveTab(allListItemTabNames)
